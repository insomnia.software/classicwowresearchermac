//
//  AppController.m
//  ClassicWoWResearcher
//
//  Created by Insomnia on 15/06/2014.
//  Copyright (c) 2014 Insomnia. All rights reserved.
//

#import "AppController.h"
#import "Website.h"

@implementation AppController

- (void)awakeFromNib
{
    [_comboBox selectItemAtIndex:0];

    websiteCheckBoxes = @[_wowheadCheckBox, _wowDbCheckBox, _allakhazamCheckBox, _thottbotCheckBox, _goblinWorkshopCheckBox, _wowWikiCheckBox, _wowpediaCheckBox, _googleClassicCheckBox, _googlePreCataCheckBox, _worldOfWarCheckBox, _mageloCheckBox, _somepageCheckBox, _wowPeakCheckBox, _shootTauriCheckBox, _feenixCheckBox, _vanillaGamingCheckBox, _valkyrieCheckBox];

    //OLD SITES
    Website *Wowhead = [[Website alloc] initWithValues:@"Wowhead" url:@"http://www.wowhead.com" objectUrl:@"/object=" itemUrl:@"/item=" npcUrl:@"/npc=" questUrl:@"/quest=" suffix:@"" nameBasedUrl:0];
    Website *WoWDb = [[Website alloc] initWithValues:@"WoWDb" url:@"http://www.wowdb.com" objectUrl:@"/objects/" itemUrl:@"/items/" npcUrl:@"/npcs/" questUrl:@"/quests/" suffix:@"" nameBasedUrl:0];
    Website *Allakhazam = [[Website alloc] initWithValues:@"Allakhazam" url:@"https://web.archive.org/web/*/http://wow.allakhazam.com" objectUrl:@"/db/object.html?wobject=" itemUrl:@"/db/item.html?witem=" npcUrl:@"/db/mob.html?wmob=" questUrl:@"/db/quest.html?wquest="  suffix:@"" nameBasedUrl:0];
    Website *Thottbot = [[Website alloc] initWithValues:@"Thottbot" url:@"https://web.archive.org/web/*/http://www.thottbot.com" objectUrl:@"/o" itemUrl:@"/i" npcUrl:@"/c" questUrl:@"/q"  suffix:@"" nameBasedUrl:0];
    Website *GoblinWorkshop = [[Website alloc] initWithValues:@"GoblinWorkshop" url:@"https://web.archive.org/web/*/http://www.goblinworkshop.com" objectUrl:@"/objects/" itemUrl:@"/items/" npcUrl:@"/creatures/" questUrl:@"/quests/" suffix:@".html" nameBasedUrl:1];
    Website *WoWWiki = [[Website alloc] initWithValues:@"WoWWiki" url:@"http://www.wowwiki.com" objectUrl:@"" itemUrl:@"/" npcUrl:@"/" questUrl:@"/Quest:" suffix:@"" nameBasedUrl:1];
    Website *Wowpedia = [[Website alloc] initWithValues:@"Wowpedia" url:@"http://www.wowpedia.org" objectUrl:@"" itemUrl:@"/" npcUrl:@"/" questUrl:@"/Quest:" suffix:@"" nameBasedUrl:1];
    Website *Googleclassicdaterange = [[Website alloc] initWithValues:@"Google (classic daterange)" url:@"http://www.google.com" objectUrl:@"/search?q=" itemUrl:@"/search?q=" npcUrl:@"/search?q=" questUrl:@"/search?q=" suffix:@"+daterange:2453169-2454101" nameBasedUrl:1];
    Website *Googleprecataclysmdaterange = [[Website alloc] initWithValues:@"Google (pre-cataclysm daterange)" url:@"http://www.google.com" objectUrl:@"/search?q=" itemUrl:@"/search?q=" npcUrl:@"/search?q=" questUrl:@"/search?q=" suffix:@"+daterange:2453169-2455480" nameBasedUrl:1];
    Website *WorldofWar = [[Website alloc] initWithValues:@"World of War" url:@"https://web.archive.org/web/*/http://wwndata.worldofwar.net" objectUrl:@"/mob.php?name=" itemUrl:@"/item.php?id=" npcUrl:@"/mob.php?name=" questUrl:@"/quest.php?id=" suffix:@"" nameBasedUrl:1];
    Website *Magelo = [[Website alloc] initWithValues:@"Magelo" url:@"http://wow.magelo.com" objectUrl:@"/en/object/" itemUrl:@"/en/item/" npcUrl:@"/en/npc/" questUrl:@"/en/quest/" suffix:@"" nameBasedUrl:0];
    Website *Somepage = [[Website alloc] initWithValues:@"Somepage" url:@"http://wow.somepage.com" objectUrl:@"/object/" itemUrl:@"/item/" npcUrl:@"/npc/" questUrl:@"/quest/" suffix:@"" nameBasedUrl:0];
    Website *WoWPeak = [[Website alloc] initWithValues:@"WoWPeak" url:@"http://www.wowpeek.com" objectUrl:@"" itemUrl:@"/item.php?itemid=" npcUrl:@"/mob.php?mobid=" questUrl:@"/quest.php?questid=" suffix:@"" nameBasedUrl:0];
    Website *shoottauri = [[Website alloc] initWithValues:@"shoot.tauri" url:@"http://shoot.tauri.hu" objectUrl:@"/?object=" itemUrl:@"/?item=" npcUrl:@"/?npc=" questUrl:@"/?quest=" suffix:@"" nameBasedUrl:0];
    
    //PRIVATE
    Website *Valkrie = [[Website alloc] initWithValues:@"Valkyrie" url:@"http://db.valkyrie-wow.org" objectUrl:@"/?object=" itemUrl:@"/?item=" npcUrl:@"/?npc=" questUrl:@"/?quest=" suffix:@"" nameBasedUrl:0];
    Website *Feenix = [[Website alloc] initWithValues:@"Feenix" url:@"http://database.wow-one.com" objectUrl:@"/?object=" itemUrl:@"/?item=" npcUrl:@"/?npc=" questUrl:@"/?quest=" suffix:@"" nameBasedUrl:0];
    Website *VanillaGaming = [[Website alloc] initWithValues:@"Vanilla Gaming" url:@"http://db.vanillagaming.org" objectUrl:@"/?object=" itemUrl:@"/?item=" npcUrl:@"/?npc=" questUrl:@"/?quest=" suffix:@"" nameBasedUrl:0];
    
    websites = [NSArray arrayWithObjects: Wowhead, WoWDb, Allakhazam, Thottbot, GoblinWorkshop, WoWWiki, Wowpedia, Googleclassicdaterange, Googleprecataclysmdaterange, WorldofWar, Magelo, Somepage, WoWPeak, shoottauri, Valkrie, Feenix, VanillaGaming, nil];
}

- (IBAction)Go:(id)sender
{
    NSString *selectedType = [_comboBox objectValueOfSelectedItem];
    
    Website *wowDb = websites[1];
    
    NSString *url = [wowDb GetUrl:selectedType id:[_idTextBox stringValue] name:NULL];
    
    NSURL *urlRequest = [NSURL URLWithString:url];
    NSError *err = nil;
    
    NSString *html = [NSString stringWithContentsOfURL:urlRequest encoding:NSUTF8StringEncoding error:&err];
    
    NSString *searchedString = html;
    NSRange   searchedRange = NSMakeRange(0, [searchedString length]);
    NSString *pattern = @"<title>(.+) - (?:.+) - (?:.+)</title>";
    NSError  *error = nil;
    
    NSString *name = @"";
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
    NSArray* matches = [regex matchesInString:searchedString options:0 range: searchedRange];
    for (NSTextCheckingResult* match in matches)
    {
        NSString* matchText = [searchedString substringWithRange:[match range]];
        NSLog(@"match: %@", matchText);
        NSRange group1 = [match rangeAtIndex:1];
        name = [searchedString substringWithRange:group1];
        NSLog(@"group1: %@", [searchedString substringWithRange:group1]);
    }
    
    if(err || [name isEqualToString:nil])
    {
        //Handle 
    }
    
    for (id checkBox in websiteCheckBoxes)
    {
        for (Website *website in websites)
        {
            if([(website.name) isEqualToString:[checkBox title]] && [checkBox state])
            {
                NSLog(@"Opening URL: %@", [website GetUrl:selectedType id:[_idTextBox stringValue] name:name]);
                [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:[website GetUrl:selectedType id:[_idTextBox stringValue] name:name]]];
                break;
            }
        }
    }
}

- (IBAction)AllOld:(id)sender
{
    for (id checkBox in websiteCheckBoxes)
    {
        if(!([[checkBox title] isEqualToString:@"Feenix"] || [[checkBox title] isEqualToString:@"Valkyrie"] || [[checkBox title] isEqualToString:@"Vanilla Gaming"]))
            [checkBox setState:1];
    }
}

- (IBAction)AllPrivate:(id)sender
{
    for (id checkBox in websiteCheckBoxes)
    {
        if([[checkBox title] isEqualToString:@"Feenix"] || [[checkBox title] isEqualToString:@"Valkyrie"] || [[checkBox title] isEqualToString:@"Vanilla Gaming"])
            [checkBox setState:1];
    }
}

- (IBAction)Clear:(id)sender
{
    for (id checkBox in websiteCheckBoxes)
    {
        [checkBox setState:0];
    }
}

- (IBAction)AboutButton:(id)sender
{
    NSAlert *alert = [[NSAlert alloc] init];
    [alert setMessageText:@"About"];
    [alert setInformativeText:@"Classic Warcraft Researcher\nMade by Insomnia\n\nLimitations:\nWowWiki and WoWPedia use the name of the quest for the URL. This causes issues for multiple quests with the same name as I am unable to predict the formatting used for the URL.\nGoblinWorkshop does not store game object data and Allakhazam uses custom object IDs.\n\nFeedback:\nFor requests for site additions on the list, bug reports or any other enquiries, contact insomnia or send me a PM on the forums."];
    [alert runModal];
}

- (NSString *)getName:(NSString *)title
{
    title = [title stringByReplacingOccurrencesOfString:@" " withString:@""];
    title = [title stringByReplacingOccurrencesOfString:@"." withString:@""];
    title = [title stringByReplacingOccurrencesOfString:@"(" withString:@""];
    title = [title stringByReplacingOccurrencesOfString:@")" withString:@""];
    title = [title stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    return title;
}

- (IBAction)comboBoxChanged:(id)sender
{
    if([[_comboBox objectValueOfSelectedItem] isEqualToString:@"Object"])
    {
        [_allakhazamCheckBox setState:0];
        [_goblinWorkshopCheckBox setState:0];
        [_allakhazamCheckBox setEnabled:NO];
        [_goblinWorkshopCheckBox setEnabled:NO];
    }
    else
    {
        [_allakhazamCheckBox setEnabled:YES];
        [_goblinWorkshopCheckBox setEnabled:YES];
    }
}
@end
