//
//  AppDelegate.h
//  ClassicWoWResearcher
//
//  Created by Insomnia on 15/06/2014.
//  Copyright (c) 2014 Insomnia. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
