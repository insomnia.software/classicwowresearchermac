//
//  Website.h
//  ClassicWoWResearcher
//
//  Created by Insomnia on 21/06/2014.
//  Copyright (c) 2014 Insomnia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+MD5.h"

@interface Website : NSObject

@property NSString *name;
@property NSString *url;
@property NSString *objectUrl;
@property NSString *itemUrl;
@property NSString *npcUrl;
@property NSString *questUrl;
@property NSString *suffix;
@property int nameBasedUrl;

- (id)initWithValues:(NSString *)name_ url:(NSString *)url_ objectUrl:(NSString *)objectUrl_ itemUrl:(NSString *)itemUrl_ npcUrl:(NSString *)npcURL_ questUrl:(NSString *)questUrl_ suffix:(NSString *)suffix_ nameBasedUrl:(int)nameBasedUrl_;

- (NSString *)GetUrl:(NSString *)selectedSearchType id:(NSString *)id_ name:(NSString *)nameOfItem;

@end
