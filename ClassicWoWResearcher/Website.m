//
//  Website.m
//  ClassicWoWResearcher
//
//  Created by Insomnia on 21/06/2014.
//  Copyright (c) 2014 Insomnia. All rights reserved.
//

#import "Website.h"

@implementation Website

- (id)initWithValues:(NSString *)name_ url:(NSString *)url_ objectUrl:(NSString *)objectUrl_ itemUrl:(NSString *)itemUrl_ npcUrl:(NSString *)npcURL_ questUrl:(NSString *)questUrl_ suffix:(NSString *)suffix_ nameBasedUrl:(int)nameBasedUrl_
{
    self = [super init];
    
    if(self)
    {
        self.name = name_;
        self.url = url_;
        self.objectUrl = objectUrl_;
        self.itemUrl = itemUrl_;
        self.npcUrl = npcURL_;
        self.questUrl = questUrl_;
        self.suffix = suffix_;
        self.nameBasedUrl = nameBasedUrl_;
    }
    
    return self;
}

- (NSString *)GetUrl:(NSString *)selectedSearchType_ id:(NSString *)id_ name:(NSString *)nameOfItem_
{
    NSString *url = @"";
    
    NSURL *urlRequest = [NSURL URLWithString:url];
    NSError *err = nil;
    
    if(!(self.nameBasedUrl))
    {
        if([selectedSearchType_ isEqualToString:@"Object"])
            return [NSString stringWithFormat:@"%@%@%@%@", self.url, self.objectUrl, id_, self.suffix];
        else if([selectedSearchType_ isEqualToString:@"Item"])
            return [NSString stringWithFormat:@"%@%@%@%@", self.url, self.itemUrl, id_, self.suffix];
        else if([selectedSearchType_ isEqualToString:@"NPC"])
            return [NSString stringWithFormat:@"%@%@%@%@", self.url, self.npcUrl, id_, self.suffix];
        else if([selectedSearchType_ isEqualToString:@"Quest"])
            return [NSString stringWithFormat:@"%@%@%@%@", self.url, self.questUrl, id_, self.suffix];
    }
    else if (!([nameOfItem_ isEqualToString:NULL]))
    {
        if([self.name isEqualToString:@"GoblinWorkshop"])
        {
            nameOfItem_ = [nameOfItem_ stringByReplacingOccurrencesOfString: @"'" withString:@""];
            nameOfItem_ = [nameOfItem_ stringByReplacingOccurrencesOfString: @"," withString:@""];
            nameOfItem_ = [nameOfItem_ stringByReplacingOccurrencesOfString: @" " withString:@"-"];
        }
        else if ([self.name hasPrefix:@"Google"])
        {
            nameOfItem_ = [nameOfItem_ stringByReplacingOccurrencesOfString: @" " withString:@"+"];
        }
        else if ([self.name isEqualToString:@"World of War"])
        {
            if([selectedSearchType_ isEqualToString:@"Object"])
                nameOfItem_ = [nameOfItem_ stringByReplacingOccurrencesOfString: @" " withString:@"+"];
            else if([selectedSearchType_ isEqualToString:@"Item"])
                nameOfItem_ = id_;
            else if([selectedSearchType_ isEqualToString:@"NPC"])
                nameOfItem_ = [nameOfItem_ stringByReplacingOccurrencesOfString: @" " withString:@"+"];
            else if([selectedSearchType_ isEqualToString:@"Quest"])
                nameOfItem_ = [id_ MD5String];
        }
        else
        {
            nameOfItem_ = [nameOfItem_ stringByReplacingOccurrencesOfString: @" " withString:@"_"];
        }
        
        if([selectedSearchType_ isEqualToString:@"Object"])
            return [NSString stringWithFormat:@"%@%@%@%@", self.url, self.objectUrl, nameOfItem_, self.suffix];
        else if([selectedSearchType_ isEqualToString:@"Item"])
            return [NSString stringWithFormat:@"%@%@%@%@", self.url, self.itemUrl, nameOfItem_, self.suffix];
        else if([selectedSearchType_ isEqualToString:@"NPC"])
            return [NSString stringWithFormat:@"%@%@%@%@", self.url, self.npcUrl, nameOfItem_, self.suffix];
        else if([selectedSearchType_ isEqualToString:@"Quest"])
            return [NSString stringWithFormat:@"%@%@%@%@", self.url, self.questUrl, nameOfItem_, self.suffix];
    }
    
    return @"";
}

@end
