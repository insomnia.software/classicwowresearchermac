//
//  main.m
//  ClassicWoWResearcher
//
//  Created by Insomnia on 15/06/2014.
//  Copyright (c) 2014 Insomnia. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
