//
//  AppController.h
//  ClassicWoWResearcher
//
//  Created by Insomnia on 15/06/2014.
//  Copyright (c) 2014 Insomnia. All rights reserved.
//

#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wdangling-else"
#endif

#import <Foundation/Foundation.h>

@interface AppController : NSObject
{
@private
    __weak NSComboBox *_comboBox;
    __weak NSPopUpButton *_popUpBox;
    __weak NSTextField *_idTextBox;
    __weak NSButton *_wowheadCheckBox;
    __weak NSButton *_wowDbCheckBox;
    __weak NSButton *_allakhazamCheckBox;
    __weak NSButton *_thottbotCheckBox;
    __weak NSButton *_goblinWorkshopCheckBox;
    __weak NSButton *_wowWikiCheckBox;
    __weak NSButton *_wowpediaCheckBox;
    __weak NSButton *_googleClassicCheckBox;
    __weak NSButton *_googlePreCataCheckBox;
    __weak NSButton *_worldOfWarCheckBox;
    __weak NSButton *_mageloCheckBox;
    __weak NSButton *_somepageCheckBox;
    __weak NSButton *_wowPeakCheckBox;
    __weak NSButton *_shootTauriCheckBox;
    __weak NSButton *_valkyrieCheckBox;
    __weak NSButton *_vanillaGamingCheckBox;
    __weak NSButton *_feenixCheckBox;
    NSArray *websiteCheckBoxes;
    NSArray *websites;
}

- (IBAction)Go:(id)sender;
- (IBAction)AllOld:(id)sender;
- (IBAction)AllPrivate:(id)sender;
- (IBAction)Clear:(id)sender;
- (IBAction)comboBoxChanged:(id)sender;
- (IBAction)AboutButton:(id)sender;
- (NSString *)getName:(NSString *)title;
- (NSString *)getUrl:(NSString *)title;

@property (weak) IBOutlet NSComboBox *comboBox;
@property (weak) IBOutlet NSButton *wowheadCheckBox;
@property (weak) IBOutlet NSButton *allakhazamCheckBox;
@property (weak) IBOutlet NSButton *wowDbCheckBox;
@property (weak) IBOutlet NSButton *thottbotCheckBox;
@property (weak) IBOutlet NSButton *goblinWorkshopCheckBox;
@property (weak) IBOutlet NSButton *wowWikiCheckBox;
@property (weak) IBOutlet NSButton *wowpediaCheckBox;
@property (weak) IBOutlet NSButton *googleClassicCheckBox;
@property (weak) IBOutlet NSButton *googlePreCataCheckBox;
@property (weak) IBOutlet NSButton *worldOfWarCheckBox;
@property (weak) IBOutlet NSButton *mageloCheckBox;
@property (weak) IBOutlet NSButton *somepageCheckBox;
@property (weak) IBOutlet NSButton *wowPeakCheckBox;
@property (weak) IBOutlet NSButton *shootTauriCheckBox;
@property (weak) IBOutlet NSButton *valkyrieCheckBox;
@property (weak) IBOutlet NSButton *vanillaGamingCheckBox;
@property (weak) IBOutlet NSButton *feenixCheckBox;
@property (weak) IBOutlet NSTextField *idTextBox;
@end
